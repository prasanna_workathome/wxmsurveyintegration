﻿using System.Threading.Tasks;
using FreshDesk.Ticket.Business.DomainModel;

namespace FreshDesk.Ticket.Business.Core
{
    public interface ISender 
    {
        Task SendMessageToQueue(WxmSurveyResponse wxmSurveyResponse);
    }
}
