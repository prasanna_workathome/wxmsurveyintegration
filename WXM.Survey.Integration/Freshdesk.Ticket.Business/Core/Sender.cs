﻿using System;
using System.Text;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using FreshDesk.Ticket.Business.DomainModel;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace FreshDesk.Ticket.Business.Core
{
    public class Sender : ISender
    {
        private readonly ILogger<ServiceBusConsumer> _logger;

        private readonly Azure.Messaging.ServiceBus.ServiceBusSender _clientSender;

        public Sender(IConfiguration configuration, ILogger<ServiceBusConsumer> logger)
        {
            var configuration1 = configuration;
            _logger = logger;

            var connectionString = configuration1.GetSection("ServiceBusConnectionString").Value;
            var queueName = configuration1.GetSection("ServiceBusQueueName").Value;
            var serviceBusClient = new ServiceBusClient(connectionString);
            _clientSender = serviceBusClient.CreateSender(queueName);
        }

        /// <summary>
        /// SendMessageToQueue
        /// </summary>
        /// <param name="wxmSurveyResponse"></param>
        /// <returns></returns>
        public async Task SendMessageToQueue(WxmSurveyResponse wxmSurveyResponse)
        {
            string messagePayload = JsonConvert.SerializeObject(wxmSurveyResponse);
           
            // Create a new message to send to the queue
            var serviceBusMessage = new ServiceBusMessage(Encoding.UTF8.GetBytes(messagePayload))
            {
                MessageId = Guid.NewGuid().ToString(),
                ContentType = "application/json"
            };

            // Send the message to the queue
            await _clientSender.SendMessageAsync(serviceBusMessage);
        }
    }
}
