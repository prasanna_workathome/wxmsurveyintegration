﻿using System.Threading.Tasks;
using FreshDesk.Ticket.Business.DomainModel;

namespace FreshDesk.Ticket.Business.Core
{
    public interface IThirdPartyApiCall
    {
        Task<Tickets> HttpPost(string url, BaseTicket ticket);
    }
}
