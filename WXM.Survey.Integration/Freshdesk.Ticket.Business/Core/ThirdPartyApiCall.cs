﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using FreshDesk.Ticket.Business.DomainModel;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace FreshDesk.Ticket.Business.Core
{
    public class ThirdPartyApiCall : IThirdPartyApiCall
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;

        public ThirdPartyApiCall(IHttpClientFactory clientFactory,ILogger<ThirdPartyApiCall> logger,IConfiguration configuration)
        {
            _clientFactory = clientFactory;
            _logger = logger;
            _configuration = configuration;
        }
        public async Task<Tickets> HttpPost(string url , BaseTicket ticket)
        {
            Tickets tickets;
            try
            {
                var sharedKey = _configuration.GetSection("SharedKey").Value;
                var client = _clientFactory.CreateClient("FreshDesk");

                HttpRequestMessage message = new HttpRequestMessage();
                message.Headers.Add("Accept", "application/json");
                //TODO need to get sharedKey
                message.Headers.Add("Authorization", sharedKey);
                message.Content = new StringContent(JsonConvert.SerializeObject(ticket), System.Text.Encoding.UTF8, "application/json");
                message.RequestUri = new Uri(url);
                message.Method = HttpMethod.Post;

                var response = await client.SendAsync(message);
                var apiResponse = await response.Content.ReadAsStringAsync();

                tickets = JsonConvert.DeserializeObject<Tickets>(apiResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }

            return tickets;
        }
    }
}
