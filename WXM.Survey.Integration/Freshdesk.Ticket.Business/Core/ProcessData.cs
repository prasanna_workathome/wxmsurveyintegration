﻿using System;
using System.Threading.Tasks;
using FreshDesk.Ticket.Business.DomainModel;
using FreshDesk.Ticket.Business.Middleware;
using FreshDesk.Ticket.Business.Repository;

namespace FreshDesk.Ticket.Business.Core
{
    public class ProcessData : IProcessData
    {
        private readonly IFreshDeskTicketRepository _freshDeskTicketRepository;

        public ProcessData(IFreshDeskTicketRepository freshDeskTicketRepository)
        {
            _freshDeskTicketRepository = freshDeskTicketRepository;
        }
        /// <summary>
        /// Handle Survey response
        /// </summary>
        /// <param name="payload"></param>
        /// <returns></returns>
        public async Task<Tickets> Process(WxmSurveyResponse payload)
        {
            Tickets response;
            try
            {
                //Note used own key to connect to service bus
                var requestBody = _freshDeskTicketRepository.ConstructTicketBaseModel(payload);
                response = await _freshDeskTicketRepository.CreateTicket(requestBody);
            }
            catch (Exception ex)
            {
                throw new AppException(ex.Message);
            }

            return response;
        }
    }
}
