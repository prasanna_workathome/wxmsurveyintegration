﻿using System;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using FreshDesk.Ticket.Business.DomainModel;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace FreshDesk.Ticket.Business.Core
{
    public class ServiceBusConsumer : IServiceBusConsumer
    {
        private readonly ILogger<ServiceBusConsumer> _logger;
        public static WxmSurveyResponse wxmSurveyResponse;

        private readonly ServiceBusClient _serviceBusClient;
        private readonly ServiceBusSender _clientSender;
        private ServiceBusProcessor _processor;
        private readonly IProcessData _processData;
        private readonly string _queueName;


        public ServiceBusConsumer(IConfiguration configuration, ILogger<ServiceBusConsumer> logger, IProcessData processData)
        {
            var configuration1 = configuration;
            _logger = logger;
            _processData = processData;

            var connectionString = configuration1.GetSection("ServiceBusConnectionString").Value;
            _queueName = configuration1.GetSection("ServiceBusQueueName").Value;
            _serviceBusClient = new ServiceBusClient(connectionString);
            _clientSender = _serviceBusClient.CreateSender(_queueName);
        }

        /// <summary>
        /// RegisterOnMessageHandlerAndReceiveMessages
        /// </summary>
        /// <returns></returns>
        public async Task RegisterOnMessageHandlerAndReceiveMessages()
        {
            try
            {
                var serviceBusProcessorOptions = new ServiceBusProcessorOptions
                {
                    MaxConcurrentCalls = 1,
                    AutoCompleteMessages = false,
                };

                _processor = _serviceBusClient.CreateProcessor(_queueName, serviceBusProcessorOptions);
                _processor.ProcessMessageAsync += ProcessMessagesAsync;
                _processor.ProcessErrorAsync += ProcessErrorAsync;
                await _processor.StartProcessingAsync().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            
        }

        /// <summary>
        /// CloseQueueAsync
        /// </summary>
        /// <returns></returns>
        public async Task CloseQueueAsync()
        {
            await _processor.CloseAsync().ConfigureAwait(false);
        }

        #region PrivateMethods

        /// <summary>
        /// DisposeAsync
        /// </summary>
        /// <returns></returns>
        public async ValueTask DisposeAsync()
        {
            if (_processor != null)
            {
                await _processor.DisposeAsync().ConfigureAwait(false);
            }

            if (_serviceBusClient != null)
            {
                await _serviceBusClient.DisposeAsync().ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Error Handling
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Task ProcessErrorAsync(ProcessErrorEventArgs arg)
        {
            _logger.LogError(arg.Exception, "Message handler encountered an exception");
            _logger.LogDebug($"- ErrorSource: {arg.ErrorSource}");
            _logger.LogDebug($"- Entity Path: {arg.EntityPath}");
            _logger.LogDebug($"- FullyQualifiedNamespace: {arg.FullyQualifiedNamespace}");

            return Task.CompletedTask;
        }
        /// <summary>
        /// Process the message received from queue 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private async Task ProcessMessagesAsync(ProcessMessageEventArgs args)
        {
            var payload = args.Message.Body.ToObjectFromJson<WxmSurveyResponse>();
            //TODO local testing read from queue 
            wxmSurveyResponse = payload;
            await _processData.Process(payload).ConfigureAwait(false);
            await args.CompleteMessageAsync(args.Message).ConfigureAwait(false);
        }

        #endregion
    }
}
