﻿using System.Threading.Tasks;

namespace FreshDesk.Ticket.Business.Core
{
    public interface IServiceBusConsumer
    {
        Task RegisterOnMessageHandlerAndReceiveMessages();
        Task CloseQueueAsync();
        ValueTask DisposeAsync();
    }
}
