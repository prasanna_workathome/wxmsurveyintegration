﻿using System.Threading.Tasks;
using FreshDesk.Ticket.Business.DomainModel;

namespace FreshDesk.Ticket.Business.Repository
{
    public interface IFreshDeskTicketRepository
    {
        Task<Tickets> CreateTicket(BaseTicket tickets);
        BaseTicket ConstructTicketBaseModel(WxmSurveyResponse wxmSurveyResponse);
    }
}
