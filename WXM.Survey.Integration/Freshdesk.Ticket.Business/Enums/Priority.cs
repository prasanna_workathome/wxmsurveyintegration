﻿namespace FreshDesk.Ticket.Business.Enums
{
    public enum Priority
    {
        ELow=1,
        Medium=2,
        High=3,
        Urgent=4,
    }
}
