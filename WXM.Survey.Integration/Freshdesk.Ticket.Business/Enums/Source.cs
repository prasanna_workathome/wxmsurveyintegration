﻿namespace FreshDesk.Ticket.Business.Enums
{
    public enum Source
    {
        Email =1,
        Portal =2,
        Phone=3,
        Chat=7,
        FeedbackWidget=9,
        OutboundEmail=10
    }
}
