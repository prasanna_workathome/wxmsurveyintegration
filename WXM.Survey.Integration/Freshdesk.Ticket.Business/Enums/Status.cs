﻿namespace FreshDesk.Ticket.Business.Enums
{
    public enum Status
    {
        Open=2,
        Pending=3,
        Resolved=4,
        Closed=5
    }
}
