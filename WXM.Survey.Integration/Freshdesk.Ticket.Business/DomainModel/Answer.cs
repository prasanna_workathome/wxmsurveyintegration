﻿using System.Collections.Generic;

namespace FreshDesk.Ticket.Business.DomainModel
{
    public class Answer
    {
        public string Id { get; set; }
        public string User { get; set; }
        public string LocationId { get; set; }
        public string ResponseDateTime { get; set; }
        public string SurveyClient { get; set; }
        public List<Responses> Responses { get; set; }
    }
}
