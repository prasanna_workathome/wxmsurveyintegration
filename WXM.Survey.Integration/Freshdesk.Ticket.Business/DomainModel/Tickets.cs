﻿using System;
using System.Collections.Generic;

namespace FreshDesk.Ticket.Business.DomainModel
{
    public class Tickets : BaseTicket
    {
        public string Type { get; set; }
        public int  Responder_id { get; set; }
        public object[] Attachments { get; set; }
        public Dictionary<string,string> Custom_fields { get; set; }
        public DateTime Due_by { get; set; }
        public int Email_config_id { get; set; }
        public DateTime Fr_due_by { get; set; }
        public int Group_id { get; set; }
        public int Product_id { get; set; }
        public DateTime Tags { get; set; }
        public int Company_id { get; set; }
        public int Internal_agent_id { get; set; }
        public int Requester_id { get; set; }

        public string Facebook_id { get; set; }
        public string Twitter_id { get; set; }
        public string Phone { get; set; }
        public string Unique_external_id { get; set; }

    }
}
