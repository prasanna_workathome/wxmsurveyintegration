﻿namespace FreshDesk.Ticket.Business.DomainModel
{
    public class Responses
    {
        public string QuestionId { get; set; }
        public string QuestionText { get; set; }
        public string TextInput { get; set; }
        public int NumberInput { get; set; }
    }
}
