﻿using FreshDesk.Ticket.Business.Enums;

namespace FreshDesk.Ticket.Business.DomainModel
{
    public class BaseTicket
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Subject { get; set; }
        public string Email { get; set; }
        public Source Source { get; set; }
        public Status Status { get; set; }
        public Priority Priority { get; set; }
        public string[] Cc_emails { get; set; }
    }
}
