﻿using Newtonsoft.Json;

namespace FreshDesk.Ticket.Business.DomainModel
{
    public class WxmSurveyResponse
   {
       public string Notification { get; set; }
       public Answer Answer { get; set; }
       [JsonProperty("archived")]
       public bool Archived { get; set; }
       public string Notes { get; set; }
       [JsonProperty("openTicket")]
        public bool OpenTicket { get; set; }
       public string DeviceId { get; set; }
    }
}

