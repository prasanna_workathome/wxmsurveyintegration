﻿using System.Threading.Tasks;
using FreshDesk.Ticket.Business.Core;
using FreshDesk.Ticket.Business.DomainModel;
using FreshDesk.Ticket.Business.Enums;
using FreshDesk.Ticket.Business.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace FreshDesk.Ticket.Business.Facade
{
    /// <summary>
    /// FreshDeskTicketFacade
    /// </summary>
    public class FreshDeskTicketFacade : IFreshDeskTicketRepository
    {
        private readonly ILogger _logger;
        private readonly IThirdPartyApiCall _thirdPartyApiCall;
        private readonly IConfiguration _configuration;

        public FreshDeskTicketFacade(ILogger<FreshDeskTicketFacade> logger, IThirdPartyApiCall thirdPartyApiCall , IConfiguration configuration)
        {
            _logger = logger;
            _thirdPartyApiCall = thirdPartyApiCall;
            _configuration = configuration;
        }
        /// <summary>
        /// CreateTicket
        /// </summary>
        /// <param name="tickets"></param>
        /// <returns></returns>
        public async Task<Tickets> CreateTicket(BaseTicket tickets)
        {
            var url = _configuration.GetSection("ApiUrl").Value;
            if (_thirdPartyApiCall != null) return await _thirdPartyApiCall.HttpPost(url, tickets);
            return null;
        }
        /// <summary>
        /// Construct Ticket BaseModel
        /// </summary>
        /// <param name="wxmSurveyResponse"></param>
        /// <returns></returns>
        public BaseTicket ConstructTicketBaseModel(WxmSurveyResponse wxmSurveyResponse)
        {
            //TODO all the information should come from API response  
            //TODO but current response model does not have have required details from WebEx system  
            string[] ccEmail = _configuration.GetSection("CC_Email").Value.Split(',');
            //This BaseTicket model is the basic data required to create ticket in FreshDesk
            var baseTicket = new BaseTicket
            {
                Description = wxmSurveyResponse.Notes,
                Email = _configuration.GetSection("Email").Value,
                Cc_emails = ccEmail,
                Subject = wxmSurveyResponse.Notes,
                Priority = Priority.High,
                Status = Status.Open
            };
            return baseTicket;
        }
    }
}
