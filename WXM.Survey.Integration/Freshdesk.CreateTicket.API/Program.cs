using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.ApplicationInsights;

namespace FreshDesk.CreateTicket.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                }).ConfigureLogging((context, builder) =>
            {
                // Providing an instrumentation key is required if you're using the
                // standalone Microsoft.Extensions.Logging.ApplicationInsights package,
                // or when you need to capture logs during application startup, such as
                // in Program.cs or Startup.cs itself.
                builder.AddApplicationInsights(
                    context.Configuration["ApplicationInsightKey"]);

                // Capture all log-level entries from Program
                builder.AddFilter<ApplicationInsightsLoggerProvider>(
                    typeof(Program).FullName, LogLevel.Trace);

                // Capture all log-level entries from Startup
                builder.AddFilter<ApplicationInsightsLoggerProvider>(
                    typeof(Startup).FullName, LogLevel.Trace);
            });
    }
}
