﻿using System.Threading.Tasks;
using FreshDesk.Ticket.Business.Core;
using FreshDesk.Ticket.Business.DomainModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace FreshDesk.CreateTicket.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FreshDeskCreateTicketController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IProcessData _processData;

        public FreshDeskCreateTicketController(ILogger<FreshDeskCreateTicketController> logger, IProcessData processData)
        {
            _logger = logger;
            _processData = processData;
        }

        //This is only for testing as Ticket creation process is happening using service bus Listener
        // GET api/<FreshDeskCreateTicketController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult>  Get(int id)
        {
            return Ok(ServiceBusConsumer.wxmSurveyResponse);
        }
    }
}
