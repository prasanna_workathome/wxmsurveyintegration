using System;
using System.Net.Http;
using FluentValidation.AspNetCore;
using FreshDesk.Ticket.Business.ActionFilter;
using FreshDesk.Ticket.Business.Core;
using FreshDesk.Ticket.Business.Facade;
using FreshDesk.Ticket.Business.Middleware;
using FreshDesk.Ticket.Business.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Polly;
using Polly.Extensions.Http;

namespace FreshDesk.CreateTicket.API
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpClient();
            services.AddSingleton<IFreshDeskTicketRepository, FreshDeskTicketFacade>();
            services.AddSingleton<IThirdPartyApiCall, ThirdPartyApiCall>();
            services.AddTransient<IServiceBusConsumer, ServiceBusConsumer>();
            services.AddSingleton<IProcessData, ProcessData>();
            services.AddControllers();
            services.AddApplicationInsightsTelemetry();

            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            //resilience and transient-fault handling capabilities
            services.AddHttpClient("FreshDesk")
                .SetHandlerLifetime(TimeSpan.FromMinutes(5))
                .AddPolicyHandler(GetRetryPolicy());

            services.AddMvc(opt =>
            {
                opt.Filters.Add(typeof(ValidatorActionFilter));
            }).AddFluentValidation(fvc => fvc.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Freshdesk.Ticket.API", Version = "v1" });
            });
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Freshdesk.Ticket.API v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // global error handler
            app.UseMiddleware<ErrorHandlerMiddleware>();

            var serviceBus = app.ApplicationServices.GetService<IServiceBusConsumer>();
            if (serviceBus != null) serviceBus.RegisterOnMessageHandlerAndReceiveMessages().GetAwaiter().GetResult();
        }
        private IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
                    // HttpRequestException, 5XX and 408  
                    .HandleTransientHttpError()
                    // 404  
                    .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.NotFound)
                    // Retry two times after delay  
                    .WaitAndRetryAsync(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)))
                ;
        }
    }
}
