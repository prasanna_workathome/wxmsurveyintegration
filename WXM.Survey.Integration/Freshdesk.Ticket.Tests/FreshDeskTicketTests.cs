using System;
using FreshDesk.Ticket.Business.Core;
using FreshDesk.Ticket.Business.DomainModel;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Wxm.ReceiveResponse.API.Controllers;

namespace FreshDesk.Ticket.Tests
{
    public class FreshDeskTicketTests
    {
        private ILogger<WxmResponseController> _logger;
        private ISender _sender;
        [SetUp]
        public void Setup()
        {
            _logger = Mock.Of<ILogger<WxmResponseController>>();
            var mockedSender = new Mock<ISender>();
            mockedSender.Setup(x => x.SendMessageToQueue(GetWxmSurveyResponse()));
            _sender = mockedSender.Object;

        }

        [Test]
        public void SaveResponseOnServiceBusQueueTest()
        {
            var successMsg = "Survey Response message has been successfully pushed to queue";
            var controller = new WxmResponseController(_logger, _sender);

            var surveyResponse = GetWxmSurveyResponse();
            var result =  controller.Post(surveyResponse).Result;

            var actualMsg = ((Microsoft.AspNetCore.Mvc.ObjectResult) result).Value;
            var statusCode = ((Microsoft.AspNetCore.Mvc.ObjectResult)result).StatusCode;

            Assert.AreEqual(successMsg, actualMsg);
            Assert.AreEqual(200, statusCode);
        }

        [Test]
        public void NullSurveyResponseTest()
        {
            try
            {
                var controller = new WxmResponseController(_logger, _sender);
                var result = controller.Post(null).Result;
            }
            catch (Exception ex)
            {
               Assert.Pass();
            }
            Assert.Fail();
        }


        private WxmSurveyResponse GetWxmSurveyResponse()
        {
            var wxmSurveyResponse = new WxmSurveyResponse
            {
                Notes = "This is Test Response"
            };

            return wxmSurveyResponse;
        }
    }
}