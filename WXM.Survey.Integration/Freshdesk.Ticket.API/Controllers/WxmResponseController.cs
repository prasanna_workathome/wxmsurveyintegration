﻿using System;
using System.Threading.Tasks;
using FreshDesk.Ticket.Business.Core;
using FreshDesk.Ticket.Business.DomainModel;
using FreshDesk.Ticket.Business.Middleware;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Wxm.ReceiveResponse.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WxmResponseController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly ISender _sender;

        public WxmResponseController(ILogger<WxmResponseController> logger, ISender sender)
        {
            _logger = logger;
            _sender = sender;
        }

        // POST api/<WxmResponseController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] WxmSurveyResponse wxmSurveyResponse)
        {
            try
            {
                var surveyResponse = wxmSurveyResponse ?? throw new AppException("Survey Response is null or invalid");
                await _sender.SendMessageToQueue(surveyResponse);
                return Ok("Survey Response message has been successfully pushed to queue");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }
    }
}
